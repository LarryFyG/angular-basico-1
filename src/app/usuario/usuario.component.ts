import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  isDisabled: boolean;
  miSaludo: string;
  miNombre: string;
  nombres: string[];
  constructor() {

  }
  agregarNombre(nombre: string): void {
    this.nombres.push(nombre);
    this.miNombre = '';
  }
  funcion2() {
    setTimeout(() => {
      this.isDisabled = true;
    }, 2000);
  }
  saludar() {
    this.miSaludo = 'Hola mundo';
  }
  setPlaceholder(): boolean {
    return this.miNombre.length === 0 || this.miNombre === undefined ;
  }

  ngOnInit() {
    this.nombres = [];
    this.isDisabled = false;
    this.miSaludo = '';
    this.miNombre = '';
    this.funcion2();
  }

}
